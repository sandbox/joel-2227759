<?php

/**
 * Expose themes as context reactions.
 */
class context_reaction_settitle extends context_reaction {
  /**
   * Editor form.
   */
  function editor_form($context) {
    $form = $this->options_form($context);

    return $form;
  }

  /**
   * Submit handler for editor form.
   */
  function editor_form_submit($context, $values) {
    return $values;
  }

  /**
   * Allow admins to provide a title
   */
  function options_form($context) {
    $values = $this->fetch_from_context($context);
    $form = array();
    $form['title'] = array(
      '#title' => t('Page title'),
      '#description' => t('Overrides the page title by calling drupal_set_title with the provided title'),
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#default_value' => isset($values['title']) ? $values['title'] : '',
    );
    return $form;
  }

  /**
   * Execute the context reaction
   */
  function execute() {
    foreach ($this->get_contexts() as $k => $v) {
      if (!empty($v->reactions[$this->plugin]['title'])) {
        $title = $v->reactions[$this->plugin]['title'];
        $title = $title == '<none>' ? '' : $title;
        if (module_exists('i18n_string')) {
          $title = i18n_string("context_set_title:$k:title", $title);
        }
        drupal_set_title($title);
      }
    }
  }
}
