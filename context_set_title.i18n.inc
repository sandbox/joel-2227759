<?php
/**
 * @file
 * Internationalization (i18n) hooks.
 */

/**
 * Implements hook_i18n_string_info()
 */
function context_set_title_i18n_string_info() {
  $groups['context_set_title'] = array(
    'title' => t('Context titles'),
    'description' => t('Context title override related strings.'),
    'format' => FALSE, // This group doesn't have strings with format
    'list' => TRUE, // This group can list all strings
  );
  return $groups;
}

/**
 * Implements hook_i18n_string_list().
 */
function context_set_title_i18n_string_list($group) {
  if ($group == 'context_set_title' || $group == 'all') {
    $strings = array();

    foreach (context_set_title_get_contexts() as $context) {
      $strings['context_set_title'][] = array(
        'title' => $context->reactions['set_title']['title']
      );
    }

    return $strings;
  }
}

/**
 * Implements hook_i18n_string_refresh().
 *
 * Refresh translations for all generated strings.
 */
function context_set_title_i18n_string_refresh($group) {
  if ($group == 'context_set_title') {
    context_set_title_i18n_update_strings();
  }
  return TRUE;
}
